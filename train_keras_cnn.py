import keras

from keras.models import model_from_json
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import GaussianNoise as GN
from keras.layers.normalization import BatchNormalization as BN
from keras.preprocessing.image import ImageDataGenerator

from utils.improcess import get_image_set
from utils.improcess import CLASSES
from utils.report import report_keras

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

from sklearn.externals import joblib
import os

batch_size = 32
epochs = 100
data_augmentation = True

RANDOM_STATE = 42
num_classes = len(CLASSES)

images, target = get_image_set()
encoder = LabelEncoder()
target_loaded = encoder.fit_transform(target) # Change class names ("up", "down", etc) to numbers (0, 1, etc).
x_train, x_test, y_train, y_test = train_test_split(images, target_loaded, test_size=0.2, shuffle=True, random_state=RANDOM_STATE)

print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')


y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255


model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), padding='same', input_shape=x_train[0].shape))

model.add(Activation('relu'))
model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(128, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Dropout(0.25))

model.add(Conv2D(128, (3, 3), padding='same'))
model.add(Activation('relu'))
model.add(Conv2D(128, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(512))
model.add(BN())
model.add(GN(0.3))
model.add(Activation('relu'))

model.add(Dense(num_classes))
model.add(Activation('softmax'))

opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])


gen = ImageDataGenerator(width_shift_range=0.1, 
                         height_shift_range=0.1, horizontal_flip=False, vertical_flip=True)

gen.fit(x_train)

train_generator = gen.flow(x_train, y_train, batch_size=batch_size)

history = model.fit_generator(train_generator,
                    epochs=epochs,
                    steps_per_epoch=50000//32,
                    verbose=1,
                    validation_data=(x_test, y_test),
                    validation_steps=10000//32)

report_keras(history, "keras_cnn")

scores = model.evaluate(x_test, y_test, verbose=1)

# serialize model to JSON
model_json = model.to_json()
with open(os.path.join("models","keras_cnn.json"), "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights(os.path.join("models","keras_cnn.h5"))
joblib.dump(encoder, os.path.join("models", "label_encoder_keras_cnn.code"))
print("Saved model to disk")


print('Test loss:', scores[0])
print('Test accuracy:', scores[1])
