# Simple Gestures
A simple application which recognizes 4 simple gestures and a general _rest_:
- Up
- Down
- Stop
- Play
- Rest

## Motivation
After my simple [first experiments](https://github.com/jorgarga/neural_network_basics) in machine learning I kept reading and learning. After a while I wanted to try something from A to Z. I wanted to learn about:
- __Adquisition__: What makes a good data set? How difficult is it to create one?
- __Processing__: Ways to make the machine learn; different algorithms and frameworks.
- __Result__: How to know if my network has learnt anything useful? Is it overfitting/underfitting?
- __Proof of concept__: Can/Could it be used in a real application?

All this in contrast to just running somebody else's Jupyter notebooks with standard (and provided) datasets.

## Prerequesites
- Python 3.5
- Numpy 1.13.3
- Scikit-learn 0.19.1
- Matplotlib 2.1.0
- Keras 2.0.9
- h5py 2.7.1
- OpenCV 3.1.0

## Install
OpenCV will have to be installed separately. The rest can be installed on any sort of virtual environment.

> git clone https://github.com/jorgarga/simple_gestures.git
> 
> cd simple_gestures
> 
> virtualenv env
> 
> source ./bin/activate
> 
> pip install -r requirements.txt


The project was developed using Ubuntu 16.04 LTS (Xenial Xerus).


## Project structure
- data: The input images for training are found here. One folder for each class.
  - down
  - play
  - rest
  - stop
  - up
- demo: Video showing the system working.
- models: The result of the training is placed here[^my_models].
- utils: Convert images into a dataset for the framework, data augmentation, etc.

[^my_models]: The models I trained with Keras are provided, because they don't take much space and they take long time to train. The model trained with Scikit-learn is not provided, because it is too heavy, but it take some minutes to train.

## Usage
### If you want to produce custom data
Just run the following command:
 >    python video_stream.py --action capture

This will capture images from your webcam and save them in the current directory. Manual classification must take place afterwards, the images have to be placed under _data_/{correct_label} folder, eg. _data_/_up_.

### If you want to train a network
- To train a neural network using Scikit-learn run:

     > python ./train_scikit_mlp.py
- To train a neural network using Keras run:

    > python ./train_keras_mlp.py
- To train a convolutional neural network using Keras run:

    > python ./train_keras_cnn.py

The result of the training processes will be saved under the _models_ folder.

### Use the recognizer on live images from the webcam
To run the multilevel perceptron trained with Scikit-learn run the following command:
> python video_stream.py --action predict_scikit_mlp

To run the multilevel perceptron trained with Keras run the following command:
> python video_stream.py --action predict_keras_mlp

To run the convolutional neural network trained with Keras run the following command:
> python video_stream.py --action predict_keras_cnn


## Comments
I found it very pleasant to work with Scikit-learn and its standarized [working flow](https://www.datacamp.com/community/blog/scikit-learn-cheat-sheet). The documentation is amazing and it helps understanding the existing [estimators](http://scikit-learn.org/stable/tutorial/machine_learning_map/index.html), what model hyperparameters exist and what they are used for. I tried many estimators until I finally decided to continue my project with a multi-layer perceptron.

Both multi-layer models (using Scikit-learn and Keras) achieve a good accuracy, but in order to improve the result one needs to use convolutional neural networks, since they take [local spatiality](https://ujjwalkarn.me/2016/08/11/intuitive-explanation-convnets/) into account.

It is very **important** to have a good dataset. My results at the beginning of the experiment were not very promising, because the images I used for training were not good enough. I used a **considerable amount of time** creating a dataset with _good_ images, _bad_ images and something in between. Then I applied some simple rotations to increment the size of my dataset. Data _augmentation_ is a big concept in machine learning[^augmentation].

[^augmentation]: Take a look at this [explanation](https://machinelearningmastery.com/image-augmentation-deep-learning-keras/) for Keras and this general [framework](https://github.com/aleju/imgaug).

I would like to thank Google for their product [Google Colaboratory](https://colab.research.google.com/), which helps us amateurs to investigate the world of machine learning[^colab].

[^colab]: Take a look at the [guide](https://medium.com/deep-learning-turkey/google-colab-free-gpu-tutorial-e113627b9f5d) to make it work.
