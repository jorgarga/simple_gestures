import keras

from keras.models import model_from_json
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.layers import GaussianNoise as GN
from keras.layers import GaussianDropout as GD
from keras.layers.normalization import BatchNormalization as BN

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from utils.improcess import get_data_set
from utils.improcess import CLASSES
from utils.report import report_keras

from sklearn.externals import joblib
import os



batch_size = 32
epochs = 100

RANDOM_STATE = 42
num_classes = len(CLASSES)

data_loaded, target_loaded = get_data_set()
encoder = LabelEncoder()
target_loaded = encoder.fit_transform(target_loaded) # Change class names ("up", "down", etc) to numbers (0, 1, etc).
x_train, x_test, y_train, y_test = train_test_split(data_loaded, target_loaded, test_size=0.2, shuffle=True, random_state=RANDOM_STATE)

print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')


y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255


model = Sequential()
model.add(Dense(1024, input_shape=(x_train[0].shape[0],)))
model.add(BN())
model.add(GN(0.2))
model.add(Activation('relu'))

model.add(Dense(512))
model.add(BN())
model.add(GN(0.3))
model.add(Activation('relu'))

model.add(Dense(256))
model.add(GD(0.1))
model.add(Activation('relu'))

model.add(Dense(128))
model.add(BN())
model.add(GN(0.3))
model.add(Activation('relu'))

model.add(Dense(64))
model.add(Activation('relu'))

model.add(Dense(num_classes, activation='softmax'))


opt = keras.optimizers.rmsprop(lr=0.001, decay=1e-6)
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])


history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_data=(x_test, y_test),
                    verbose=1,
                    shuffle=True)

report_keras(history, "keras_mlp")

scores = model.evaluate(x_test, y_test, verbose=1)

# serialize model to JSON
model_json = model.to_json()
with open(os.path.join("models","keras_mlp.json"), "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights(os.path.join("models","keras_mlp.h5"))
joblib.dump(encoder, os.path.join("models", "label_encoder_keras_mlp.code"))
print("Saved model to disk")


print('Test loss:', scores[0])
print('Test accuracy:', scores[1])
