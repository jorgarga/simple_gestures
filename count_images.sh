#!/bin/bash

FOLDER="data"

echo ""
UP="$(ls -l $FOLDER/up/ | wc -l)"
echo "          UP:  ${UP}"

DOWN="$(ls -l $FOLDER/down/ | wc -l)"
echo "     +  DOWN:  ${DOWN}"

PLAY="$(ls -l $FOLDER/play/ | wc -l)"
echo "     +  PLAY:  ${PLAY}"

STOP="$(ls -l $FOLDER/stop/ | wc -l)"
echo "     +  STOP:  ${STOP}"

REST="$(ls -l $FOLDER/rest/ | wc -l)"
echo "     +  REST:  ${REST}"

echo "----------------------"

TOTAL=$(($UP + $DOWN + $PLAY + $STOP + $REST))
echo "TOTAL IMAGES: ${TOTAL}"
echo ""
