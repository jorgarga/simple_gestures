# -*- coding: utf-8 -*-

from utils.decorators import crono
from utils.improcess import get_data_set
from utils.report import report_scikit

from sklearn.neural_network import MLPClassifier

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

RANDOM_STATE = 42

def train_mlperceptron(X_train, y_train, X_test, y_test, le):
    """This function will create a perceptron model and save it to disk for later use."""

    input_layer = len(X_train[0])
    hidden1  = 1000
    hidden2  =  900
    hidden3  =  800
    hidden4  =  700
    hidden5  =  600
    hidden6  =  500
    hidden7  =  400
    hidden8  =  300
    hidden9  =  200
    hidden10 =  100
    hidden11 =   50

    model = MLPClassifier(hidden_layer_sizes=(input_layer, hidden1, hidden2, hidden3, hidden4, hidden5, hidden6,
                                              hidden7, hidden8, hidden9, hidden10, hidden11),
                          activation="relu", solver="adam", alpha=0.0001, learning_rate="adaptive", verbose=True,
                          learning_rate_init=0.001, max_iter=2000, shuffle=True, random_state=RANDOM_STATE)

    model.fit(X_train, y_train)
    model_name = "{}.model".format("MLPerperceptron")
    report_scikit(model, model_name, X_test, y_test, le)



@crono
def mlperceptron_model():
    data_loaded, target_loaded = get_data_set()
    encoder = LabelEncoder()
    target_loaded = encoder.fit_transform(target_loaded) # Change class names ("up", "down", etc) to numbers (0, 1, etc).
    X_train, X_test, y_train, y_test = train_test_split(data_loaded, target_loaded, test_size=0.8, shuffle=True, random_state=RANDOM_STATE)
    train_mlperceptron(X_train, y_train, X_test, y_test, encoder)



if __name__ == "__main__":
    mlperceptron_model()
