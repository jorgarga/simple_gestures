import datetime
import functools

def crono(func):
    # Implementation of the decorator.
    @functools.wraps(func)  # Good practice based on https://dbader.org/blog/python-decorators
    def wrapper(*args, **kwargs):
        start = datetime.datetime.now()
        original_result = func(*args, **kwargs)
        end = datetime.datetime.now()
        print("TRACE: Function '{}' took {} to execute.".format(func.__name__,end - start))
        return original_result

    return wrapper

