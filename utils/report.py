import numpy as np
import itertools

from sklearn.externals import joblib
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from utils.improcess import RESIZE_IMAGE

import os


RANDOM_STATE = 42

def report_keras(history, model_name):
    plt.plot(history.history["acc"])
    plt.plot(history.history["val_acc"])
    plt.title("Model accuracy")
    plt.ylabel("Accuracy")
    plt.xlabel("Epoch")
    plt.legend(["Train", "Test"], loc="upper left")
    plt.savefig(os.path.join(os.getcwd(), "models", model_name + "_model_accuracy.png"))

    plt.clf()
    plt.plot(history.history["loss"])
    plt.plot(history.history["val_loss"])
    plt.title("Model loss")
    plt.ylabel("Loss")
    plt.xlabel("Epoch")
    plt.legend(["Train", "Test"], loc="upper left")
    plt.savefig(os.path.join(os.getcwd(), "models", model_name + "_model_loss.png"))


def report_scikit(model, model_name, X_test, y_test, le):
    joblib.dump(model, os.path.join("models", model_name))
    joblib.dump(le, os.path.join("models", "label_encoder_{}.code".format(model_name)))
    target_predicted = model.predict(X_test)
    score = accuracy_score(y_test, target_predicted)
    print("{} - Score: {}".format(model_name, score))
    print(classification_report(y_test, target_predicted, target_names=le.classes_))
    y_test = np.asarray(y_test)
    misclassified = np.where(y_test != target_predicted)
    mis = len(misclassified[0])
    total = len(y_test)
    pct = mis / total
    print("Number of misclassified items/items in y_test: {}/{} = {}".format(mis, total, pct))
    print("Misclassified items: {}".format(misclassified))
    plot_confusion_matrix(y_test, target_predicted, le.classes_, os.path.join(os.getcwd(), "models"), model_name)
    plot_network_weights(model, os.path.join(os.getcwd(), "models"), model_name)


def plot_confusion_matrix(y_test, y_pred, class_names, path, model_name):
    """
    This code has been taken from the following tutorial:
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    """
    
    def do_plot_confusion_matrix(cm, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')

    # Compute confusion matrix
    cnf_matrix = confusion_matrix(y_test, y_pred)
    np.set_printoptions(precision=2)

    # Plot non-normalized confusion matrix
    plt.figure()
    do_plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, without normalization')
    plt.savefig(os.path.join(path, model_name + '_confusion_matrix_not_normalized.png'))

    # Plot normalized confusion matrix
    plt.figure()
    do_plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                          title='Normalized confusion matrix')
    plt.savefig(os.path.join(path, model_name + '_confusion_matrix_normalized.png'))
    #plt.show()


def plot_network_weights(mlp, path, model_name):
    # Source from: http://scikit-learn.org/stable/auto_examples/neural_networks/plot_mnist_filters.html
    fig, axes = plt.subplots(20, 20)
    # use global min / max to ensure all weights are shown on the same scale
    vmin, vmax = mlp.coefs_[0].min(), mlp.coefs_[0].max()
    for coef, ax in zip(mlp.coefs_[0].T, axes.ravel()):
        ax.matshow(coef.reshape(RESIZE_IMAGE, RESIZE_IMAGE), cmap=plt.cm.gray, vmin=.5 * vmin,
                   vmax=.5 * vmax)
        ax.set_xticks(())
        ax.set_yticks(())

    plt.savefig(os.path.join(path, model_name + '_network_weights.png'))
    #plt.show()


