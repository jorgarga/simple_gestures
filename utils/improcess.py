import os
import pickle

import hashlib
import glob
import numpy as np
import time
import cv2
from skimage.feature import hog
from utils.decorators import crono

import datetime

RESIZE_IMAGE = 40

DATA_FOLDER = os.path.join(os.getcwd(), "data")
CLASSES = [name for name in os.listdir(DATA_FOLDER) if os.path.isdir(os.path.join(DATA_FOLDER, name))]

def get_prepared_image(im):
    im = im.flatten()
    return im


def resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    """
    The source code for this function is taken from:
    https://github.com/jrosebr1/imutils/blob/master/imutils/convenience.py
    """
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image

    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation=inter)
    return resized


def rotate(image, angle, center = None, scale = 1.0):
    """
    The source code for this function is taken from:
    https://github.com/jrosebr1/imutils/blob/master/imutils/convenience.py
    """
    (h, w) = image.shape[:2]

    if center is None:
        center = (w // 2, h // 2)

    M = cv2.getRotationMatrix2D(center, angle, scale)
    rotated = cv2.warpAffine(image, M, (w, h))
    return rotated


def get_data_set():
    data_loaded, target_loaded = build_dataset(os.path.join(os.getcwd(),
                                                            "data"))
    return data_loaded, target_loaded


@crono
def build_dataset(path):
    """
    This function will build and return a dataset from the available folders.
    One folder is expected for each class:
        - data/up
        - data/down
        - data/stop
        - data/rest
        - data/play

    The dataset consists of images and the class they belong to.
    If there is a cached file created it will be used.
    If not, all the image files will be processed and a cache file will be created.
    """

    if os.path.isfile(os.path.join(path, "data_loaded.pickle")) and \
       os.path.isfile(os.path.join(path, "target_loaded.pickle")):
        data_loaded = pickle.load( open(os.path.join(path, "data_loaded.pickle"), "rb" ))
        target_loaded = pickle.load( open(os.path.join(path, "target_loaded.pickle"), "rb" ))

        return np.array(data_loaded), np.array(target_loaded)
    else:
        return generate_flatened_dataset(path)


def generate_flatened_dataset(path):
    images = []
    target = []

    report_image = 0

    for folder in CLASSES:
        imagePaths = sorted(glob.glob(path + "/{}/*.png".format(folder)))
        for image in imagePaths:
            read_im = cv2.imread(image)
            color_im = resize(read_im, width=RESIZE_IMAGE)
            grey_im = cv2.cvtColor(color_im, cv2.COLOR_BGR2GRAY)

            # Data augmentation. Rotate images and change orientation.
            rotations = [-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6]
            orientations = { "normal": grey_im, "flip": cv2.flip(grey_im, 1)}
            file_images = sum([len(files) for r, d, files in os.walk(path)])

            # Used for terminal process reporting.
            all_images = file_images * len(rotations) * len(orientations)

            for orientation, image in orientations.items():
                for rotation in rotations:
                    im_rotated = rotate(image, rotation)
                    flattened = get_prepared_image(im_rotated)

                    images.append(flattened)
                    target.append(folder)

                    report_image += 1
                    if report_image % 2000 != 0: continue

                    pct = (report_image * 100) / all_images
                    print("Working on image {:>6}/{}. {:^8.5}%.".format(report_image,
                                                                        all_images,
                                                                        pct))

    pickle.dump(images, open(os.path.join(path, "data_loaded.pickle"), "wb" ))
    pickle.dump(target, open(os.path.join(path, "target_loaded.pickle"), "wb" ))
    return np.array(images), np.array(target)

    

def get_image_set():
    images, target = read_images(os.path.join(os.getcwd(), "data"))
    return images, target


@crono
def read_images(path):
    """
    This function is very similar to 'build_dataset'.
    It will be used by Keras when working with convolutional networks.
    """

    if os.path.isfile(os.path.join(path, "keras_images.pickle")) and os.path.isfile(os.path.join(path, "keras_target.pickle")):
        images = pickle.load( open(os.path.join(path, "keras_images.pickle"), "rb" ))
        target = pickle.load( open(os.path.join(path, "keras_target.pickle"), "rb" ))

        return np.array(images), np.array(target)

    else:
        return generate_color_dataset(path)


def generate_color_dataset(path):
    images = []
    target = []

    report_image = 0

    for folder in CLASSES:
        imagePaths = sorted(glob.glob(path + "/{}/*.png".format(folder)))
        for image in imagePaths:
            read_im = cv2.imread(image)
            color_im = resize(read_im, width=RESIZE_IMAGE)

            # Data augmentation. Rotate images and change orientation.
            rotations = [-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6]
            orientations = { "normal": color_im, "flip": cv2.flip(color_im, 1)}
            file_images = sum([len(files) for r, d, files in os.walk(path)])

            # Used for terminal process reporting.
            all_images = file_images * len(rotations) * len(orientations)

            for orientation, image in orientations.items():
                for rotation in rotations:
                    im_rotated = rotate(image, rotation)

                    images.append(im_rotated)
                    target.append(folder)

                    report_image += 1
                    if report_image % 2000 != 0: continue

                    pct = (report_image * 100) / all_images
                    print("Working on image {:>6}/{}. {:^8.5}%.".format(report_image,
                                                                        all_images,
                                                                        pct))

    pickle.dump(images, open(os.path.join(path, "keras_images.pickle"), "wb" ))
    pickle.dump(target, open(os.path.join(path, "keras_target.pickle"), "wb" ))

    return np.array(images), np.array(target)



def save_image(new_image, image_number=None):
    current_time = time.strftime("%Y_%m_%d_%H_%M_%S")
    hash_object = hashlib.sha1(current_time.encode('utf-8'))
    hex_digest = hash_object.hexdigest()

    filename = "{}.png".format(hex_digest)
    cv2.imwrite(filename, new_image)
    print("Saving file {}".format(filename))

    text = ""
    if image_number:
        text = "Saving image {}".format(image_number)
    else:
        text = "Saving image"

    command = 'espeak -s140 -p50 "' + text + '"'
    print(command)
    os.system(command)
