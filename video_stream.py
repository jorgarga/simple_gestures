# -*- coding: utf-8 -*-

import os
import cv2
import time
import numpy as np
from sklearn.externals import joblib
from keras.models import model_from_json

from utils.improcess import resize, save_image, get_prepared_image
from utils.improcess import RESIZE_IMAGE

import time
import argparse
import sys


def load_mlp_model():
    try:
        model = joblib.load(os.path.join("models", "MLPerperceptron.model"))
        le = joblib.load(os.path.join("models", "label_encoder_MLPerperceptron.model.code"))
        print("Loaded model from disk")
        return model, le
    except Exception as ex:
        print("Something went wrong loading the MLP model: {}".format(ex))
        exit()

def load_keras_model(model):
    try:
        filename = "keras_" + str(model)
        label_encoder_filename = "label_encoder_keras_" + str(model)

        # load json and create model
        json_file = open(os.path.join("models", filename + ".json"), "r")
        model_json = json_file.read()
        json_file.close()

        model = model_from_json(model_json)
        # load weights into new model
        model.load_weights(os.path.join("models", filename + ".h5"))
        le = joblib.load(os.path.join("models", label_encoder_filename + ".code"))
        print("Loaded model from disk")
        return model, le
    except Exception as ex:
        print("Something went wrong loading the Keras model: {}".format(ex))
        exit()



def video_stream(mode=None):
    if mode is None: return

    model = None
    le = None

    boundary = 0.9
    min_percent = 0.1
    roi_window_name = "ROI"
    counter = 0
    image_number = 1
    text_color = (0, 0, 255)

    if mode.upper() == "PREDICT_SCIKIT_MLP":
        model, le = load_mlp_model()
    elif mode.upper() == "PREDICT_KERAS_CNN":
        model, le = load_keras_model("cnn")
    elif mode.upper() == "PREDICT_KERAS_MLP":
        model, le = load_keras_model("mlp")
    
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_BRIGHTNESS, 0.3)

    fgbg = cv2.createBackgroundSubtractorMOG2()

    while cap.isOpened():
        ret, inputImage = cap.read()

        k=cv2.waitKey(30) & 0xff
        if k==27:
            break

        fgmask = fgbg.apply(inputImage)

        size_x = 150
        size_y = 150
        coord_x = 240
        coord_y = 80

        roi = fgmask[coord_y:coord_y + size_y, coord_x:coord_x + size_x]

        cv2.rectangle(inputImage, (coord_x, coord_y), (coord_x + size_x, coord_y + size_y), (0, 0, 255), 8)
        cv2.imshow('inputImage', inputImage)

        roi = resize(roi, width=500)
        percentage = cv2.countNonZero(roi)/len(roi)
        prev_predicted = None

        if percentage > min_percent:
            if mode.upper() == "CAPTURE":
                print("percentage {}%".format(percentage))
                cv2.imshow(roi_window_name, roi)

                counter = counter + 1
                if counter > 80:
                    save_image(roi, image_number)
                    counter = 0
                    image_number = image_number + 1

            elif mode.upper() == "PREDICT_SCIKIT_MLP":
                cv2.imshow(roi_window_name, roi)
                im = get_prepared_image(resize(roi, width=RESIZE_IMAGE))

                target_predicted = model.predict(im.reshape(1, -1))[0]
                target_prob = model.predict_proba(im.reshape(1, -1))[0]
                predicted_class = le.inverse_transform([target_predicted])[0]

                win_score = target_prob.max()
                win_class = predicted_class

                predicted_value = win_class if win_score > boundary else prev_predicted if prev_predicted is not None else "rest"
                prev_predicted = predicted_value
                print("The model predicted {:^5} with probability {:^15.10}. We will predict {:>4}.".format(win_class,
                                                                                                            win_score,
                                                                                                            predicted_value))
                predicted = "Predicted: {}".format(predicted_value)
                cv2.putText(inputImage, predicted, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color, 3)
                cv2.imshow('inputImage', inputImage)

            elif mode.upper() == "PREDICT_KERAS_CNN":
                cv2.imshow(roi_window_name, roi)
                im = resize(roi, width=RESIZE_IMAGE)
                im = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR) # Convert to RGB. The convolution expects a color pic.
                im = im.astype('float32')                 # The correct type is needed
                im /= 255                                 # as well as normalization. So that
                                                          # Keras can make good predictions.
                im = np.expand_dims(im, axis=0)

                target_predicted = model.predict_classes(im)[0]
                predicted_class = le.inverse_transform([target_predicted])[0]

                win_score = model.predict_proba(im).max()
                win_class = predicted_class

                predicted_value = win_class if win_score > boundary else prev_predicted if prev_predicted is not None else "rest"
                prev_predicted = predicted_value

                print("The model predicted {:^5} with probability {:^15.10}. We will predict {:>4}.".format(win_class,
                                                                                                            win_score,
                                                                                                            predicted_value))

                predicted = "Predicted: {}".format(predicted_value)
                cv2.putText(inputImage, predicted, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color, 3)
                cv2.imshow('inputImage', inputImage)

            elif mode.upper() == "PREDICT_KERAS_MLP":
                cv2.imshow(roi_window_name, roi)
                im = resize(roi, width=RESIZE_IMAGE)
                im = im.flatten()

                im = im.astype('float32')      # The correct type is needed
                im /= 255                      # as well as normalization. So that
                im = np.array(im)[np.newaxis]  # Keras can make good predictions.

                target_predicted = model.predict_classes(im)[0]
                predicted_class = le.inverse_transform([target_predicted])[0]

                print("The model predicted {}".format(predicted_class))

                predicted = "Predicted: {}".format(predicted_class)
                cv2.putText(inputImage, predicted, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color, 3)
                cv2.imshow('inputImage', inputImage)

            else:
                pass

        else:
            cv2.destroyWindow(roi_window_name)


    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    # Call script like this:
    #     python video_stream.py --action capture
    #     python video_stream.py --action predict_scikit_mlp
    #     python video_stream.py --action predict_keras_mlp
    #     python video_stream.py --action predict_keras_cnn

    parser = argparse.ArgumentParser()
    parser.add_argument("--action", help="run or stop", nargs="?", choices=("capture",
                                                                            "predict_scikit_mlp",
                                                                            "predict_keras_cnn",
                                                                            "predict_keras_mlp"))
    args = parser.parse_args()
    video_stream(args.action)

